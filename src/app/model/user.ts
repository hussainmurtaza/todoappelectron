export class User {
  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }
  private _id: string;
  private _fullName: string;
  private _email: string;
  private _password: string;
  private _confirmPassword: string;

  get confirmPassword(): string {
    return this._confirmPassword;
  }

  set confirmPassword(value: string) {
    this._confirmPassword = value;
  }

  get fullName(): string {
    return this._fullName;
  }

  set fullName(value: string) {
    this._fullName = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }
}
