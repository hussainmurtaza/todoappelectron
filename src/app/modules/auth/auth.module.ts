import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from '../../components/dashboard/dashboard.component';
import {MyTaskComponent} from '../../components/my-task/my-task.component';
import {CreateTodoItemComponent} from '../../components/create-todo-item/create-todo-item.component';
import {AuthRoutingModuleModule} from '../../routes/auth-routing-module/auth-routing-module.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SearchComponent} from '../../components/search/search.component';
import {TodoItemListComponent} from '../../components/todo-item-list/todo-item-list.component';
import {StatusPipe} from '../../pipes/status.pipe';
import {PaginatorComponent} from '../../components/paginator/paginator.component';
import {ChangePasswordComponent} from '../../components/change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModuleModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [DashboardComponent, MyTaskComponent, CreateTodoItemComponent, SearchComponent, TodoItemListComponent, PaginatorComponent, StatusPipe, ChangePasswordComponent]
})
export class AuthModule { }
