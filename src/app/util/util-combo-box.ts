
export class UtilComboBox {
  get display(): string {
    return this._display;
  }

  set display(value: string) {
    this._display = value;
  }
  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
  }
  private _value: string;
  private _display: string;
}
