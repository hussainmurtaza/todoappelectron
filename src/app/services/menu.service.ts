import {Injectable, NgZone} from '@angular/core';
import {ElectronService} from '../providers/electron.service';
import MenuItemConstructorOptions = Electron.MenuItemConstructorOptions;
import {Router} from '@angular/router';
import BrowserWindow = Electron.BrowserWindow;
import Tray = Electron.Tray;
import {StorageService} from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private tray: Tray = null;
  private smallWindow: BrowserWindow = null;
  constructor(private zone: NgZone, private service: ElectronService, private router: Router, private storage: StorageService) {

  }

  public setLoggedInMenu() {
    this.service.ipcRenderer.send('loginEvent');
  }
  public setGuestMenu() {
    this.service.ipcRenderer.send('logOutEvent');
  }
  public setLoggedInMenu2() {


    const template = [
      {
        label: 'Account',
        submenu: [
          {
            label: 'Change Password',
            click: p => {
              this.router.navigateByUrl('/dashboard/changePassword');
            }
          },
          {
            label: 'LogOut',
            click: p => {
              this.service.remote.getCurrentWebContents().send('logOutEvent', '/dashboard/changePassword');
            }
          },
          { type: 'separator' },
          {label: 'Exit', 'role': 'quit'}
        ]
      }, {
        label: 'Debug',
        submenu: [
          {
            label: 'Dev Tools',
            click: p => {
              this.service.remote.getCurrentWebContents().openDevTools();
            }
          },
        ]
      },
      {
        label: 'Dashboard',
        submenu: [
          {
            label: 'All Tasks',
            click: p => {
              this.zone.run(() => {
                this.router.navigateByUrl('/dashboard/dashboard');
              });
            }
          },
          {
            label: 'My Tasks',
            click: p => {
              this.zone.run(() => {
                this.router.navigateByUrl('/dashboard/tasks');
            });
            }
          },
        ]
      },

    ];
    const m = this.service.remote.Menu.buildFromTemplate(template as MenuItemConstructorOptions[]);
    this.service.remote.Menu.setApplicationMenu(m);
    this.addTray();
  }

  private addTray(): void {
    if (this.smallWindow === null) {
      this.smallWindow = new this.service.remote.BrowserWindow({
        width: 250,
        height: 310,
        show: false,
        fullscreenable: false,
        resizable: false,
        alwaysOnTop: true,
        darkTheme: true,
        hasShadow: true,
        movable: false,
        frame: false
      });
      this.smallWindow.loadURL('http://localhost:4200/#/dashboard/dashboard?isTray=a');
    }
    if (this.tray === null) {
      this.tray = new this.service.remote.Tray('logo-angular.jpg');
      this.tray.setTitle('Todo Tray Icon');
      this.tray.setToolTip('Click on me');
      const position = this.getWindowPosition();
      this.smallWindow.setPosition(position.x, position.y, false);
      this.tray.addListener('click', () => {
        if (this.smallWindow.isVisible()) {
          this.smallWindow.hide();
          return;
        } else {
          this.smallWindow.show();
          return;
        }
      });
    }

  }
  getWindowPosition() {
    const windowBounds = this.smallWindow.getBounds();
    const trayBounds = this.tray.getBounds();

    // Center window horizontally below the tray icon
    const x = Math.round(trayBounds.x + (trayBounds.width / 2) - (windowBounds.width / 2));

    // Position window 4 pixels vertically below the tray icon
    const y = Math.round(trayBounds.y - windowBounds.height);

    return {x: x, y: y};
  }
  private removeTray(): void {
    this.tray.destroy();
    this.tray = null;
  }
  public setGuestMenu2() {
    const template = [
      {
        label: 'Account',
        submenu: [
          {
            label: 'Login',
            click: p => {
              this.zone.run(() => {
              this.router.navigateByUrl( '/account/login');
              });
            }
          },
          {
            label: 'Register',
            click: p => {
              this.router.navigateByUrl( '/account/register');
            }
          },
          {
            label: 'Forget Password',
            click: p => {
              this.router.navigateByUrl('/account/forgetPassword');
            }
          },
          { type: 'separator' },
          {label: 'Exit', 'role': 'quit'}
        ]
      }, {
        label: 'Debug',
        submenu: [
          {
            label: 'Dev Tools',
            click: p => {
              this.service.remote.getCurrentWebContents().openDevTools();
            }
          },
        ]
      }];

    const m = this.service.remote.Menu.buildFromTemplate(template as MenuItemConstructorOptions[]);
    this.service.remote.Menu.setApplicationMenu(m);
    this.removeTray();
  }


}
