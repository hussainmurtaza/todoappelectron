import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot} from '@angular/router';
import {UserService} from './user.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeActivateService implements CanDeactivate<any> {

  constructor(private _userService: UserService, private router: Router) { }

  canDeactivate(component: any, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._userService.isAuthenticated()) {
      this.router.navigateByUrl('/dashboard/dashboard').then(p => {
        console.log(p);
      });
      return false;
    }
    return true;
  }

}
