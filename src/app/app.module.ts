import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import {AlertComponent} from './components/alert/alert.component';
import {NavMenuComponent} from './components/nav-menu/nav-menu.component';
import {DeleteTodoItemComponent} from './components/modals/delete-todo-item/delete-todo-item.component';
import {EditTodoItemComponent} from './components/modals/edit-todo-item/edit-todo-item.component';
import {AssignTodoItemComponent} from './components/modals/assign-todo-item/assign-todo-item.component';
import {CloseTodoItemComponent} from './components/modals/close-todo-item/close-todo-item.component';
import {DashboardNavMenuComponent} from './components/dashboard-nav-menu/dashboard-nav-menu.component';
import {MyHttpInterceptor} from './components/interceptor/my-http-interceptor';
import {AlertService} from './services/alert.service';
import {BsDropdownModule, CollapseModule, ModalModule, TooltipModule} from 'ngx-bootstrap';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    WebviewDirective,
    AppComponent,
    NavMenuComponent,
    AlertComponent,
    DeleteTodoItemComponent,
    EditTodoItemComponent,
    AssignTodoItemComponent,
    CloseTodoItemComponent,
    DashboardNavMenuComponent,
  ],
  entryComponents: [CloseTodoItemComponent, DeleteTodoItemComponent, EditTodoItemComponent, AssignTodoItemComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService, AlertService, {  provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
