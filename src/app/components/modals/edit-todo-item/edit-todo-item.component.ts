import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../../services/alert.service';
import {BsModalRef} from 'ngx-bootstrap';
import {TodoService} from '../../../services/todo.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ResponseResult} from '../../../resultModel/responseResult';
import {UtilResponse} from '../../../util/util-response';
import {Todo} from '../../../model/todo';

@Component({
  selector: 'app-edit-todo-item',
  templateUrl: './edit-todo-item.component.html',
})
export class EditTodoItemComponent implements OnInit {

  id: number;
  item: Todo;
  createForm: FormGroup;
  titleControl: FormControl;
  statusControl: FormControl;

  constructor(public bsModalRef: BsModalRef, private service: TodoService, private alert: AlertService) {
    this.titleControl = new FormControl('', [Validators.required, Validators.pattern('^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$')]);
    this.statusControl = new FormControl('', [Validators.required, Validators.min(1), Validators.max(3)]);
    this.createForm = new FormGroup({
      title: this.titleControl,
      status: this.statusControl
    });
  }

  ngOnInit() {
    this.service.Get(this.id).subscribe(result => {
      const data = result as ResponseResult;
      this.item = data.result as Todo;
      this.titleControl.setValue(this.item.title);
      this.statusControl.setValue(this.item.status);
    }, ex => {
      console.error(ex);
    });
  }

  onSubmit(form: FormGroup) {
    if (form.valid) {
      const t = this.item;
      t.title = this.titleControl.value;
      t.status = this.statusControl.value;
      this.service.Update(t).subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.alert.success('Item Updated');
          this.titleControl.reset();
          this.statusControl.reset();
          this.bsModalRef.hide();
          const index = this.service.list.findIndex(x => x.id === this.id);
          if (index > -1) {
            const it = data.result as Todo;
            this.service.list[index].title = it.title;
            this.service.list[index].status = it.status;
            this.service.list[index].createdAt = it.createdAt;
            this.service.list[index].updatedAt = it.updatedAt;
          }
        } else if (data.message === UtilResponse.BAD || data.message === UtilResponse.ERROR) {
          data.error.map(p => {
            this.alert.warn(p);
          });
        }
      });
    }
  }
}
