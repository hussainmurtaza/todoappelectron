import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {StorageService} from '../../services/storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isCollapsed = false;
  constructor(public service: UserService, private storage: StorageService, private router: Router) { }

  ngOnInit() {
  }

  LogOut() {
    this.service.authenticated = false;
    this.storage.clear();
    this.router.navigate(['']);
  }
}
