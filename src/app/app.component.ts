import {Component, OnInit} from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuService} from './services/menu.service';
import {UserService} from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  constructor(public electronService: ElectronService,
    private translate: TranslateService, private router: Router, private  route: ActivatedRoute, private service: MenuService, private userService: UserService) {

    translate.setDefaultLang('en');
    if (this.userService.isAuthenticated()) {
      this.service.setLoggedInMenu();
    } else {
      this.service.setGuestMenu();
    }
  }
  ngOnInit(): void {
    this.electronService.ipcRenderer.on('routeEvent', (sender, args) => {
      this.router.navigateByUrl(args).then(p => {
        console.log(args + ' : ' + p);
      });
    });
    this.electronService.ipcRenderer.on('logOutEvent', (sender, args) => {
      this.userService.Logout();
      this.service.setGuestMenu();
      this.router.navigateByUrl('/account/login');
    });

  }
}
