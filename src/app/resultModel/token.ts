export class Token {
  get endDate(): Date {
    return this._endDate;
  }

  set endDate(value: Date) {
    this._endDate = value;
  }
  private _token: string;
  private _endDate: Date;

  get token(): string {
    return this._token;
  }

  set token(value: string) {
    this._token = value;
  }


}
