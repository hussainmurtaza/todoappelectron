import { app, BrowserWindow, screen } from 'electron';
import * as path from 'path';
import * as url from 'url';
import { Menu, Tray, IpcMain } from 'electron';
import MenuItemConstructorOptions = Electron.MenuItemConstructorOptions;
const ipc = require('electron').ipcMain;

let win, serve;
let tray = null;
let smallWindow = null;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height
  });

  if (serve) {
    require('electron-reload')(__dirname, {
     electron: require(`${__dirname}/node_modules/electron`)});
    win.loadURL('http://localhost:4200');
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  // win.webContents.openDevTools();

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });
  ipc.on('loginEvent', setLoggedInMenu);
  ipc.on('logOutEvent', setGuestMenu);
}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });



} catch (e) {
  // Catch Error
  // throw e;
}

function setLoggedInMenu() {
  const template = [
    {
      label: 'Account',
      submenu: [
        {
          label: 'Change Password',
          click: () => {
            win.webContents.send('routeEvent', '/dashboard/changePassword');
          }
        },
        {
          label: 'LogOut',
          click: p => {
            win.webContents.send('logOutEvent', '/dashboard/changePassword');
          }
        },
        { type: 'separator' },
        {label: 'Exit', 'role': 'quit'}
      ]
    }, {
      label: 'Debug',
      submenu: [
        {
          label: 'Dev Tools',
          click: p => {
            win.webContents.openDevTools();
          }
        },
      ]
    },
    {
      label: 'Dashboard',
      submenu: [
        {
          label: 'All Tasks',
          click: p => {
            win.webContents.send('routeEvent', '/dashboard/dashboard');
          }
        },
        {
          label: 'My Tasks',
          click: p => {
            win.webContents.send('routeEvent', '/dashboard/tasks');

          }
        },
      ]
    },

  ];
  const m = Menu.buildFromTemplate(template as MenuItemConstructorOptions[]);
  Menu.setApplicationMenu(m);
  addTray();
}

function setGuestMenu() {
  const template = [
    {
      label: 'Account',
      submenu: [
        {
          label: 'Login',
          click: p => {
            win.webContents.send('routeEvent', '/account/login');
          }
        },
        {
          label: 'Register',
          click: p => {
            win.webContents.send('routeEvent', '/account/register');
          }
        },
        {
          label: 'Forget Password',
          click: p => {
            win.webContents.send('routeEvent', '/account/forgetPassword');
          }
        },
        { type: 'separator' },
        {label: 'Exit', 'role': 'quit'}
      ]
    }, {
      label: 'Debug',
      submenu: [
        {
          label: 'Dev Tools',
          click: p => {
            win.webContents.openDevTools();
          }
        },
      ]
    }];

  const m = Menu.buildFromTemplate(template as MenuItemConstructorOptions[]);
  Menu.setApplicationMenu(m);
  removeTray();
}
function addTray(): void {
  if (smallWindow === null) {
  smallWindow = new BrowserWindow({
    width: 250,
    height: 310,
    show: false,
    fullscreenable: false,
    resizable: false,
    alwaysOnTop: true,
    darkTheme: true,
    hasShadow: true,
    movable: false,
    frame: false
  });
  smallWindow.loadURL('http://localhost:4200/#/dashboard/dashboard?isTray=a');
}
if (tray === null) {
  tray = new Tray('logo-angular.jpg');
  tray.setTitle('Todo Tray Icon');
  tray.setToolTip('Click on me');
  const position = getWindowPosition();
  smallWindow.setPosition(position.x, position.y, false);
  tray.addListener('click', () => {
    if (smallWindow.isVisible()) {
      smallWindow.hide();
      return;
    } else {
      smallWindow.show();
      return;
    }
  });
}

}

function getWindowPosition() {
  const windowBounds = smallWindow.getBounds();
  const trayBounds = tray.getBounds();

  // Center window horizontally below the tray icon
  const x = Math.round(trayBounds.x + (trayBounds.width / 2) - (windowBounds.width / 2));

  // Position window 4 pixels vertically below the tray icon
  const y = Math.round(trayBounds.y - windowBounds.height);

  return {x: x, y: y};
}
function removeTray() {
  if (tray != null) {
    tray.destroy();
    tray = null;
  }
}
